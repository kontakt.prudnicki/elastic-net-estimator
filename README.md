# Elastic net estimator

This repository contains a Python script which, based on a file containing the input data, generates predicted values using IterativeImputer and the ElasticNet estimator.
